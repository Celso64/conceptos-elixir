import { useState, useEffect } from "react";

function useSintomas() {
  const [isLoading, setLoading] = useState(true);
  const [id, setId] = useState(0);
  const [sintoma, setSintoma] = useState(null);
  const [sintomasElegidos, setSintomasElegidos] = useState([]);
  const [sintomasLista, setSintomasLista] = useState([]);
  const [todosSintomas, setTodosSintomas] = useState([]);

  useEffect(() => {
    setLoading(true);
    fetch("http://localhost:4000/api/sintoma")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((json) => {
        setTodosSintomas(json.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    const sintomasNoElegidos = todosSintomas.filter(
      (sintoma) =>
        !sintomasElegidos.some((elegido) => elegido.name === sintoma.name)
    );

    setSintomasLista(sintomasNoElegidos);
  }, [sintomasElegidos, todosSintomas]);

  const handleInputChange = (data) => {
    setSintoma(data);
  };

  const handleInputSubmit = () => {
    if (sintoma) {
      if (!sintomasElegidos.includes(sintoma)) {
        const nuevo = { id: "" + id, name: sintoma.name };
        setId(id + 1);
        setSintomasElegidos([...sintomasElegidos, nuevo]);
        setSintoma(null); // Limpiar el campo de entrada
      }
    } else {
      console.log("No hay síntoma seleccionado");
    }
  };

  const handleInputDelete = (id) => {
    setSintomasElegidos(sintomasElegidos.filter((i) => i.id !== id));
  };

  return {
    isLoading,
    sintomasLista,
    sintoma,
    sintomasElegidos,
    handleInputChange,
    handleInputSubmit,
    handleInputDelete,
  };
}

export default useSintomas;
