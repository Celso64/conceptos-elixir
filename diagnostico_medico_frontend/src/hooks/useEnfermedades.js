import { useState, useEffect } from "react";

function useEnfermedades(sintomas) {
  const enfermedadURL =
    "http://localhost:4000/api/enfermedades/por_sintomas?sintomas=";

  const [enfermedades, setEnfermedades] = useState([]);

  useEffect(() => {
    const sintomasLista = sintomas
      .map((i) => i.name)
      .map((s) => s.replace(/\s/g, "%20"))
      .join();

    fetch(enfermedadURL.concat(sintomasLista))
      .then((data) => data.json())
      .then((data) => setEnfermedades(data.data))
      .catch((error) => console.log(error));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sintomas]);

  return {
    enfermedades,
  };
}

export default useEnfermedades;
