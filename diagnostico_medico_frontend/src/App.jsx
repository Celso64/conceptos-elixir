import useSintomas from "./hooks/useSintomas";
import Sintomas from "./components/sintomas/Sintomas";
import Enfermedades from "./components/enfermedades/Enfermedades";
import EntradaSintoma from "./components/EntradaSintoma";
import CircularProgress from "@mui/material/CircularProgress";
import "./App.css";
import useEnfermedades from "./hooks/useEnfermedades";

function App() {
  const {
    isLoading,
    sintomasLista,
    sintoma,
    sintomasElegidos,
    handleInputChange,
    handleInputSubmit,
    handleInputDelete,
  } = useSintomas();

  const { enfermedades } = useEnfermedades(sintomasElegidos);

  return (
    <>
      <div className="app_container">
        <h1>Diagnostico Medico</h1>

        {!isLoading ? (
          <EntradaSintoma
            listaSintomas={sintomasLista}
            entrada={sintoma}
            setEntrada={handleInputChange}
            handleClick={handleInputSubmit}
          />
        ) : (
          <CircularProgress color="success" />
        )}

        <div className="tablas">
          <Sintomas
            className="sintomas"
            sintomas={sintomasElegidos}
            handleDelete={handleInputDelete}
          />

          {enfermedades ? (
            <Enfermedades
              className="enfermedades"
              enfermedades={enfermedades}
            />
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
}

export default App;
