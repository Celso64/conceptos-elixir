import PropTypes from "prop-types";
import Sintoma from "./Sintoma";
import "./sintoma.css";

Sintomas.propTypes = {
  sintomas: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
    })
  ),
  handleDelete: PropTypes.func,
};

export default function Sintomas({ sintomas, handleDelete }) {
  return (
    <div className="seccion_sintomas">
      <h2>Sintomas ({sintomas.length})</h2>
      <div className="lista_sintomas">
        {sintomas.map((item) => (
          <Sintoma key={item.id} sintoma={item} handleDelete={handleDelete} />
        ))}
      </div>
    </div>
  );
}
