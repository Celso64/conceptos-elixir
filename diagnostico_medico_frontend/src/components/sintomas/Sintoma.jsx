import PropTypes from "prop-types";
import Chip from "@mui/material/Chip";

Sintoma.propTypes = {
  sintoma: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }),
  handleDelete: PropTypes.func,
};

export default function Sintoma({ sintoma, handleDelete }) {
  return (
    <Chip
      size="medium"
      label={sintoma.name}
      color="success"
      variant="outlined"
      onDelete={() => handleDelete(sintoma.id)}
    />
  );
}
