import PropTypes from "prop-types";

Enfermedad.propTypes = {
  enfermedad: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }),
};

export default function Enfermedad({ enfermedad }) {
  return <div>{enfermedad.name}</div>;
}
