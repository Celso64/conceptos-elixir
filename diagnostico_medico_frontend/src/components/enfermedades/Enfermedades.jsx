import PropTypes from "prop-types";
import Enfermedad from "./Enfermedad";

Enfermedades.propTypes = {
  enfermedades: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
    })
  ),
};

export default function Enfermedades({ enfermedades }) {
  return (
    <div className="seccion_enfermedades">
      <h2>Enfermedades ({enfermedades.length})</h2>
      <div className="lista_enfermedades">
        {enfermedades.map((item) => (
          <Enfermedad key={item.id} enfermedad={item} />
        ))}
      </div>
    </div>
  );
}
