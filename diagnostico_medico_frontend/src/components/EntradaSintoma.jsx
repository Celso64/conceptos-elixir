import PropTypes from "prop-types";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import "../App.css";

EntradaSintoma.propTypes = {
  listaSintomas: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
    }).isRequired
  ),
  entrada: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }),
  setEntrada: PropTypes.func,
  handleClick: PropTypes.func,
};

export default function EntradaSintoma({
  listaSintomas,
  entrada,
  setEntrada,
  handleClick,
}) {
  const handleChange = (event, newValue) => {
    setEntrada(newValue);
  };

  return (
    <div className="container">
      <Autocomplete
        options={listaSintomas}
        getOptionLabel={(option) => option.name}
        value={entrada}
        onChange={handleChange}
        renderInput={(params) => <TextField {...params} label="Sintomas" />}
        isOptionEqualToValue={(option, value) => option.id === value?.id}
        sx={{
          width: 300,
          "& .MuiInputBase-root": {
            color: "white",
          },
          "& .MuiOutlinedInput-root": {
            "& fieldset": {
              borderColor: "white",
            },
            "&:hover fieldset": {
              borderColor: "white",
            },
            "&.Mui-focused fieldset": {
              borderColor: "white",
            },
          },
          "& .MuiInputLabel-root": {
            color: "white",
          },
        }}
      />
      <button type="button" onClick={handleClick}>
        Agregar
      </button>
    </div>
  );
}
