defmodule DiagnosticoMedico.DiagnosticoTest do
  use DiagnosticoMedico.DataCase

  alias DiagnosticoMedico.Diagnostico

  describe "sintomas" do
    alias DiagnosticoMedico.Diagnostico.Sintoma

    import DiagnosticoMedico.DiagnosticoFixtures

    @invalid_attrs %{name: nil}

    test "list_sintomas/0 returns all sintomas" do
      sintoma = sintoma_fixture()
      assert Diagnostico.list_sintomas() == [sintoma]
    end

    test "get_sintoma!/1 returns the sintoma with given id" do
      sintoma = sintoma_fixture()
      assert Diagnostico.get_sintoma!(sintoma.id) == sintoma
    end

    test "create_sintoma/1 with valid data creates a sintoma" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Sintoma{} = sintoma} = Diagnostico.create_sintoma(valid_attrs)
      assert sintoma.name == "some name"
    end

    test "create_sintoma/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Diagnostico.create_sintoma(@invalid_attrs)
    end

    test "update_sintoma/2 with valid data updates the sintoma" do
      sintoma = sintoma_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Sintoma{} = sintoma} = Diagnostico.update_sintoma(sintoma, update_attrs)
      assert sintoma.name == "some updated name"
    end

    test "update_sintoma/2 with invalid data returns error changeset" do
      sintoma = sintoma_fixture()
      assert {:error, %Ecto.Changeset{}} = Diagnostico.update_sintoma(sintoma, @invalid_attrs)
      assert sintoma == Diagnostico.get_sintoma!(sintoma.id)
    end

    test "delete_sintoma/1 deletes the sintoma" do
      sintoma = sintoma_fixture()
      assert {:ok, %Sintoma{}} = Diagnostico.delete_sintoma(sintoma)
      assert_raise Ecto.NoResultsError, fn -> Diagnostico.get_sintoma!(sintoma.id) end
    end

    test "change_sintoma/1 returns a sintoma changeset" do
      sintoma = sintoma_fixture()
      assert %Ecto.Changeset{} = Diagnostico.change_sintoma(sintoma)
    end
  end

  describe "enfermedades" do
    alias DiagnosticoMedico.Diagnostico.Enfermedad

    import DiagnosticoMedico.DiagnosticoFixtures

    @invalid_attrs %{name: nil}

    test "list_enfermedades/0 returns all enfermedades" do
      enfermedad = enfermedad_fixture()
      assert Diagnostico.list_enfermedades() == [enfermedad]
    end

    test "get_enfermedad!/1 returns the enfermedad with given id" do
      enfermedad = enfermedad_fixture()
      assert Diagnostico.get_enfermedad!(enfermedad.id) == enfermedad
    end

    test "create_enfermedad/1 with valid data creates a enfermedad" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Enfermedad{} = enfermedad} = Diagnostico.create_enfermedad(valid_attrs)
      assert enfermedad.name == "some name"
    end

    test "create_enfermedad/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Diagnostico.create_enfermedad(@invalid_attrs)
    end

    test "update_enfermedad/2 with valid data updates the enfermedad" do
      enfermedad = enfermedad_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Enfermedad{} = enfermedad} = Diagnostico.update_enfermedad(enfermedad, update_attrs)
      assert enfermedad.name == "some updated name"
    end

    test "update_enfermedad/2 with invalid data returns error changeset" do
      enfermedad = enfermedad_fixture()
      assert {:error, %Ecto.Changeset{}} = Diagnostico.update_enfermedad(enfermedad, @invalid_attrs)
      assert enfermedad == Diagnostico.get_enfermedad!(enfermedad.id)
    end

    test "delete_enfermedad/1 deletes the enfermedad" do
      enfermedad = enfermedad_fixture()
      assert {:ok, %Enfermedad{}} = Diagnostico.delete_enfermedad(enfermedad)
      assert_raise Ecto.NoResultsError, fn -> Diagnostico.get_enfermedad!(enfermedad.id) end
    end

    test "change_enfermedad/1 returns a enfermedad changeset" do
      enfermedad = enfermedad_fixture()
      assert %Ecto.Changeset{} = Diagnostico.change_enfermedad(enfermedad)
    end
  end

  describe "enfermedades_sintomas" do
    alias DiagnosticoMedico.Diagnostico.EnfermedadSintoma

    import DiagnosticoMedico.DiagnosticoFixtures

    @invalid_attrs %{}

    test "list_enfermedades_sintomas/0 returns all enfermedades_sintomas" do
      enfermedad_sintoma = enfermedad_sintoma_fixture()
      assert Diagnostico.list_enfermedades_sintomas() == [enfermedad_sintoma]
    end

    test "get_enfermedad_sintoma!/1 returns the enfermedad_sintoma with given id" do
      enfermedad_sintoma = enfermedad_sintoma_fixture()
      assert Diagnostico.get_enfermedad_sintoma!(enfermedad_sintoma.id) == enfermedad_sintoma
    end

    test "create_enfermedad_sintoma/1 with valid data creates a enfermedad_sintoma" do
      valid_attrs = %{}

      assert {:ok, %EnfermedadSintoma{} = enfermedad_sintoma} = Diagnostico.create_enfermedad_sintoma(valid_attrs)
    end

    test "create_enfermedad_sintoma/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Diagnostico.create_enfermedad_sintoma(@invalid_attrs)
    end

    test "update_enfermedad_sintoma/2 with valid data updates the enfermedad_sintoma" do
      enfermedad_sintoma = enfermedad_sintoma_fixture()
      update_attrs = %{}

      assert {:ok, %EnfermedadSintoma{} = enfermedad_sintoma} = Diagnostico.update_enfermedad_sintoma(enfermedad_sintoma, update_attrs)
    end

    test "update_enfermedad_sintoma/2 with invalid data returns error changeset" do
      enfermedad_sintoma = enfermedad_sintoma_fixture()
      assert {:error, %Ecto.Changeset{}} = Diagnostico.update_enfermedad_sintoma(enfermedad_sintoma, @invalid_attrs)
      assert enfermedad_sintoma == Diagnostico.get_enfermedad_sintoma!(enfermedad_sintoma.id)
    end

    test "delete_enfermedad_sintoma/1 deletes the enfermedad_sintoma" do
      enfermedad_sintoma = enfermedad_sintoma_fixture()
      assert {:ok, %EnfermedadSintoma{}} = Diagnostico.delete_enfermedad_sintoma(enfermedad_sintoma)
      assert_raise Ecto.NoResultsError, fn -> Diagnostico.get_enfermedad_sintoma!(enfermedad_sintoma.id) end
    end

    test "change_enfermedad_sintoma/1 returns a enfermedad_sintoma changeset" do
      enfermedad_sintoma = enfermedad_sintoma_fixture()
      assert %Ecto.Changeset{} = Diagnostico.change_enfermedad_sintoma(enfermedad_sintoma)
    end
  end
end
