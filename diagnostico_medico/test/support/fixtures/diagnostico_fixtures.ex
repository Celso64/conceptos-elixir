defmodule DiagnosticoMedico.DiagnosticoFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `DiagnosticoMedico.Diagnostico` context.
  """

  @doc """
  Generate a unique sintoma name.
  """
  def unique_sintoma_name, do: "some name#{System.unique_integer([:positive])}"

  @doc """
  Generate a sintoma.
  """
  def sintoma_fixture(attrs \\ %{}) do
    {:ok, sintoma} =
      attrs
      |> Enum.into(%{
        name: unique_sintoma_name()
      })
      |> DiagnosticoMedico.Diagnostico.create_sintoma()

    sintoma
  end

  @doc """
  Generate a unique enfermedad name.
  """
  def unique_enfermedad_name, do: "some name#{System.unique_integer([:positive])}"

  @doc """
  Generate a enfermedad.
  """
  def enfermedad_fixture(attrs \\ %{}) do
    {:ok, enfermedad} =
      attrs
      |> Enum.into(%{
        name: unique_enfermedad_name()
      })
      |> DiagnosticoMedico.Diagnostico.create_enfermedad()

    enfermedad
  end

  @doc """
  Generate a enfermedad_sintoma.
  """
  def enfermedad_sintoma_fixture(attrs \\ %{}) do
    {:ok, enfermedad_sintoma} =
      attrs
      |> Enum.into(%{

      })
      |> DiagnosticoMedico.Diagnostico.create_enfermedad_sintoma()

    enfermedad_sintoma
  end
end
