defmodule DiagnosticoMedicoWeb.EnfermedadSintomaControllerTest do
  use DiagnosticoMedicoWeb.ConnCase

  import DiagnosticoMedico.DiagnosticoFixtures

  alias DiagnosticoMedico.Diagnostico.EnfermedadSintoma

  @create_attrs %{

  }
  @update_attrs %{

  }
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all enfermedades_sintoma", %{conn: conn} do
      conn = get(conn, ~p"/api/enfermedades_sintoma")
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create enfermedad_sintoma" do
    test "renders enfermedad_sintoma when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/api/enfermedades_sintoma", enfermedad_sintoma: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/enfermedades_sintoma/#{id}")

      assert %{
               "id" => ^id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/api/enfermedades_sintoma", enfermedad_sintoma: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update enfermedad_sintoma" do
    setup [:create_enfermedad_sintoma]

    test "renders enfermedad_sintoma when data is valid", %{conn: conn, enfermedad_sintoma: %EnfermedadSintoma{id: id} = enfermedad_sintoma} do
      conn = put(conn, ~p"/api/enfermedades_sintoma/#{enfermedad_sintoma}", enfermedad_sintoma: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, ~p"/api/enfermedades_sintoma/#{id}")

      assert %{
               "id" => ^id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, enfermedad_sintoma: enfermedad_sintoma} do
      conn = put(conn, ~p"/api/enfermedades_sintoma/#{enfermedad_sintoma}", enfermedad_sintoma: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete enfermedad_sintoma" do
    setup [:create_enfermedad_sintoma]

    test "deletes chosen enfermedad_sintoma", %{conn: conn, enfermedad_sintoma: enfermedad_sintoma} do
      conn = delete(conn, ~p"/api/enfermedades_sintoma/#{enfermedad_sintoma}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/enfermedades_sintoma/#{enfermedad_sintoma}")
      end
    end
  end

  defp create_enfermedad_sintoma(_) do
    enfermedad_sintoma = enfermedad_sintoma_fixture()
    %{enfermedad_sintoma: enfermedad_sintoma}
  end
end
