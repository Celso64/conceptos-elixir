defmodule DiagnosticoMedicoWeb.SintomaControllerTest do
  use DiagnosticoMedicoWeb.ConnCase

  import DiagnosticoMedico.DiagnosticoFixtures

  alias DiagnosticoMedico.Diagnostico.Sintoma

  @create_attrs %{
    name: "some name"
  }
  @update_attrs %{
    name: "some updated name"
  }
  @invalid_attrs %{name: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all sintomas", %{conn: conn} do
      conn = get(conn, ~p"/api/sintomas")
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create sintoma" do
    test "renders sintoma when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/api/sintomas", sintoma: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/sintomas/#{id}")

      assert %{
               "id" => ^id,
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/api/sintomas", sintoma: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update sintoma" do
    setup [:create_sintoma]

    test "renders sintoma when data is valid", %{conn: conn, sintoma: %Sintoma{id: id} = sintoma} do
      conn = put(conn, ~p"/api/sintomas/#{sintoma}", sintoma: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, ~p"/api/sintomas/#{id}")

      assert %{
               "id" => ^id,
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, sintoma: sintoma} do
      conn = put(conn, ~p"/api/sintomas/#{sintoma}", sintoma: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete sintoma" do
    setup [:create_sintoma]

    test "deletes chosen sintoma", %{conn: conn, sintoma: sintoma} do
      conn = delete(conn, ~p"/api/sintomas/#{sintoma}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/sintomas/#{sintoma}")
      end
    end
  end

  defp create_sintoma(_) do
    sintoma = sintoma_fixture()
    %{sintoma: sintoma}
  end
end
