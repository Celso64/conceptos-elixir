defmodule DiagnosticoMedicoWeb.EnfermedadControllerTest do
  use DiagnosticoMedicoWeb.ConnCase

  import DiagnosticoMedico.DiagnosticoFixtures

  alias DiagnosticoMedico.Diagnostico.Enfermedad

  @create_attrs %{
    name: "some name"
  }
  @update_attrs %{
    name: "some updated name"
  }
  @invalid_attrs %{name: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all enfermedades", %{conn: conn} do
      conn = get(conn, ~p"/api/enfermedades")
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create enfermedad" do
    test "renders enfermedad when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/api/enfermedades", enfermedad: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/enfermedades/#{id}")

      assert %{
               "id" => ^id,
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/api/enfermedades", enfermedad: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update enfermedad" do
    setup [:create_enfermedad]

    test "renders enfermedad when data is valid", %{conn: conn, enfermedad: %Enfermedad{id: id} = enfermedad} do
      conn = put(conn, ~p"/api/enfermedades/#{enfermedad}", enfermedad: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, ~p"/api/enfermedades/#{id}")

      assert %{
               "id" => ^id,
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, enfermedad: enfermedad} do
      conn = put(conn, ~p"/api/enfermedades/#{enfermedad}", enfermedad: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete enfermedad" do
    setup [:create_enfermedad]

    test "deletes chosen enfermedad", %{conn: conn, enfermedad: enfermedad} do
      conn = delete(conn, ~p"/api/enfermedades/#{enfermedad}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/enfermedades/#{enfermedad}")
      end
    end
  end

  defp create_enfermedad(_) do
    enfermedad = enfermedad_fixture()
    %{enfermedad: enfermedad}
  end
end
