defmodule DiagnosticoMedico.Repo.Migrations.CreateEnfermedadesSintomas do
  use Ecto.Migration

  def change do
    create table(:enfermedades_sintomas, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :enfermedad_id, references(:enfermedades, on_delete: :nothing, type: :binary_id)
      add :sintoma_id, references(:sintomas, on_delete: :nothing, type: :binary_id)

      timestamps(type: :utc_datetime)
    end

    create index(:enfermedades_sintomas, [:enfermedad_id])
    create index(:enfermedades_sintomas, [:sintoma_id])
  end
end
