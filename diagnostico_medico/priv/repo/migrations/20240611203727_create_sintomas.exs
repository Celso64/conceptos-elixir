defmodule DiagnosticoMedico.Repo.Migrations.CreateSintomas do
  use Ecto.Migration

  def change do
    create table(:sintomas, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string

      timestamps(type: :utc_datetime)
    end

    create unique_index(:sintomas, [:name])
  end
end
