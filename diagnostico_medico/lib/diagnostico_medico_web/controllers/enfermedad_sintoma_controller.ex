defmodule DiagnosticoMedicoWeb.EnfermedadSintomaController do
  use DiagnosticoMedicoWeb, :controller

  alias DiagnosticoMedico.Diagnostico
  alias DiagnosticoMedico.Diagnostico.EnfermedadSintoma

  action_fallback DiagnosticoMedicoWeb.FallbackController

  def index(conn, _params) do
    enfermedades_sintoma = Diagnostico.list_enfermedades_sintomas()
    render(conn, :index, enfermedades_sintoma: enfermedades_sintoma)
  end

  def create(conn, %{"enfermedad_sintoma" => enfermedad_sintoma_params}) do
    with {:ok, %EnfermedadSintoma{} = enfermedad_sintoma} <- Diagnostico.create_enfermedad_sintoma(enfermedad_sintoma_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/enfermedadsintoma/#{enfermedad_sintoma}")
      |> render(:show, enfermedad_sintoma: enfermedad_sintoma)
    end
  end

  def show(conn, %{"id" => id}) do
    enfermedad_sintoma = Diagnostico.get_enfermedad_sintoma!(id)
    render(conn, :show, enfermedad_sintoma: enfermedad_sintoma)
  end

  def update(conn, %{"id" => id, "enfermedad_sintoma" => enfermedad_sintoma_params}) do
    enfermedad_sintoma = Diagnostico.get_enfermedad_sintoma!(id)

    with {:ok, %EnfermedadSintoma{} = enfermedad_sintoma} <- Diagnostico.update_enfermedad_sintoma(enfermedad_sintoma, enfermedad_sintoma_params) do
      render(conn, :show, enfermedad_sintoma: enfermedad_sintoma)
    end
  end

  def delete(conn, %{"id" => id}) do
    enfermedad_sintoma = Diagnostico.get_enfermedad_sintoma!(id)

    with {:ok, %EnfermedadSintoma{}} <- Diagnostico.delete_enfermedad_sintoma(enfermedad_sintoma) do
      send_resp(conn, :no_content, "")
    end
  end
end
