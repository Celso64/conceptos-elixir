defmodule DiagnosticoMedicoWeb.SintomaJSON do
  alias DiagnosticoMedico.Diagnostico.Sintoma

  @doc """
  Renders a list of sintomas.
  """
  def index(%{sintomas: sintomas}) do
    %{data: for(sintoma <- sintomas, do: data(sintoma))}
  end

  @doc """
  Renders a single sintoma.
  """
  def show(%{sintoma: sintoma}) do
    %{data: data(sintoma)}
  end

  defp data(%Sintoma{} = sintoma) do
    %{
      id: sintoma.id,
      name: sintoma.name
    }
  end
end
