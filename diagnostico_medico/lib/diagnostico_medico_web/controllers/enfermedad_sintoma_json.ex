defmodule DiagnosticoMedicoWeb.EnfermedadSintomaJSON do
  alias DiagnosticoMedico.Diagnostico.EnfermedadSintoma

  @doc """
  Renders a list of enfermedades_sintoma.
  """
  def index(%{enfermedades_sintoma: enfermedades_sintoma}) do
    %{data: for(enfermedad_sintoma <- enfermedades_sintoma, do: data(enfermedad_sintoma))}
  end

  @doc """
  Renders a single enfermedad_sintoma.
  """
  def show(%{enfermedad_sintoma: enfermedad_sintoma}) do
    %{data: data(enfermedad_sintoma)}
  end

  defp data(%EnfermedadSintoma{} = enfermedad_sintoma) do
    %{
      id: enfermedad_sintoma.id,
      enfermedad_id: enfermedad_sintoma.enfermedad_id,
      sintoma_id: enfermedad_sintoma.sintoma_id
    }
  end
end
