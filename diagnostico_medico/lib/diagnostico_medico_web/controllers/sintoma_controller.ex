defmodule DiagnosticoMedicoWeb.SintomaController do
  use DiagnosticoMedicoWeb, :controller

  alias DiagnosticoMedico.Diagnostico
  alias DiagnosticoMedico.Diagnostico.Sintoma

  action_fallback DiagnosticoMedicoWeb.FallbackController

  def index(conn, _params) do
    sintomas = Diagnostico.list_sintomas()
    render(conn, :index, sintomas: sintomas)
  end

  def create(conn, %{"sintoma" => sintoma_params}) do
    with {:ok, %Sintoma{} = sintoma} <- Diagnostico.create_sintoma(sintoma_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/sintomas/#{sintoma}")
      |> render(:show, sintoma: sintoma)
    end
  end

  def show(conn, %{"id" => id}) do
    sintoma = Diagnostico.get_sintoma!(id)
    render(conn, :show, sintoma: sintoma)
  end

  def update(conn, %{"id" => id, "sintoma" => sintoma_params}) do
    sintoma = Diagnostico.get_sintoma!(id)

    with {:ok, %Sintoma{} = sintoma} <- Diagnostico.update_sintoma(sintoma, sintoma_params) do
      render(conn, :show, sintoma: sintoma)
    end
  end

  def delete(conn, %{"id" => id}) do
    sintoma = Diagnostico.get_sintoma!(id)

    with {:ok, %Sintoma{}} <- Diagnostico.delete_sintoma(sintoma) do
      send_resp(conn, :no_content, "")
    end
  end
end
