defmodule DiagnosticoMedicoWeb.EnfermedadJSON do
  alias DiagnosticoMedico.Diagnostico.Enfermedad

  @doc """
  Renders a list of enfermedades.
  """
  def index(%{enfermedades: enfermedades}) do
    %{data: for(enfermedad <- enfermedades, do: data(enfermedad))}
  end

  @doc """
  Renders a single enfermedad.
  """
  def show(%{enfermedad: enfermedad}) do
    %{data: data(enfermedad)}
  end

  defp data(%Enfermedad{} = enfermedad) do
    %{
      id: enfermedad.id,
      name: enfermedad.name
    }
  end
end
