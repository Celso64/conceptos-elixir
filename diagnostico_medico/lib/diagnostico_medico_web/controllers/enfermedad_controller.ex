defmodule DiagnosticoMedicoWeb.EnfermedadController do
  use DiagnosticoMedicoWeb, :controller

  alias DiagnosticoMedico.Diagnostico
  alias DiagnosticoMedico.Diagnostico.Enfermedad

  action_fallback DiagnosticoMedicoWeb.FallbackController

  def index(conn, _params) do
    enfermedades = Diagnostico.list_enfermedades()
    render(conn, :index, enfermedades: enfermedades)
  end
  
 # ACA CAMBIE
  def por_sintomas(conn, %{"sintomas" => sintomas}) do
    sintomas_list = parse_sintomas(sintomas)
    enfermedades = Diagnostico.get_enfermedades_por_sintomas(sintomas_list)
    render(conn, DiagnosticoMedicoWeb.EnfermedadJSON, "index.json", %{enfermedades: enfermedades})
  end

  defp parse_sintomas(sintomas) when is_binary(sintomas) do
    sintomas
    |> String.split(",")
    |> Enum.map(&String.trim/1)
  end
  # ---

  def create(conn, %{"enfermedad" => enfermedad_params}) do
    with {:ok, %Enfermedad{} = enfermedad} <- Diagnostico.create_enfermedad(enfermedad_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/enfermedades/#{enfermedad}")
      |> render(:show, enfermedad: enfermedad)
    end
  end

  def show(conn, %{"id" => id}) do
    enfermedad = Diagnostico.get_enfermedad!(id)
    render(conn, :show, enfermedad: enfermedad)
  end

  def update(conn, %{"id" => id, "enfermedad" => enfermedad_params}) do
    enfermedad = Diagnostico.get_enfermedad!(id)

    with {:ok, %Enfermedad{} = enfermedad} <- Diagnostico.update_enfermedad(enfermedad, enfermedad_params) do
      render(conn, :show, enfermedad: enfermedad)
    end
  end

  def delete(conn, %{"id" => id}) do
    enfermedad = Diagnostico.get_enfermedad!(id)

    with {:ok, %Enfermedad{}} <- Diagnostico.delete_enfermedad(enfermedad) do
      send_resp(conn, :no_content, "")
    end
  end
end
