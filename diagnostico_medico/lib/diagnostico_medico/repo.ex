defmodule DiagnosticoMedico.Repo do
  use Ecto.Repo,
    otp_app: :diagnostico_medico,
    adapter: Ecto.Adapters.Postgres
end
