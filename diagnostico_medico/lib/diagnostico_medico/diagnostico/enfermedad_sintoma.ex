defmodule DiagnosticoMedico.Diagnostico.EnfermedadSintoma do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "enfermedades_sintomas" do

    field :enfermedad_id, :binary_id
    field :sintoma_id, :binary_id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(enfermedad_sintoma, attrs) do
    enfermedad_sintoma
    |> cast(attrs, [:enfermedad_id, :sintoma_id])
    |> validate_required([:enfermedad_id, :sintoma_id])
  end
end
