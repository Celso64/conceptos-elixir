defmodule DiagnosticoMedico.Diagnostico.Enfermedad do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "enfermedades" do
    field :name, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(enfermedad, attrs) do
    enfermedad
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end

end
