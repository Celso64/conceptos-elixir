defmodule DiagnosticoMedico.Diagnostico do
  @moduledoc """
  The Diagnostico context.
  """

  import Ecto.Query, warn: false
  alias DiagnosticoMedico.Repo

  alias DiagnosticoMedico.Diagnostico.Sintoma

  @doc """
  Returns the list of sintomas.

  ## Examples

      iex> list_sintomas()
      [%Sintoma{}, ...]

  """
  def list_sintomas do
    Repo.all(Sintoma)
  end

  @doc """
  Gets a single sintoma.

  Raises `Ecto.NoResultsError` if the Sintoma does not exist.

  ## Examples

      iex> get_sintoma!(123)
      %Sintoma{}

      iex> get_sintoma!(456)
      ** (Ecto.NoResultsError)

  """
  def get_sintoma!(id), do: Repo.get!(Sintoma, id)

  @doc """
  Creates a sintoma.

  ## Examples

      iex> create_sintoma(%{field: value})
      {:ok, %Sintoma{}}

      iex> create_sintoma(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_sintoma(attrs \\ %{}) do
    %Sintoma{}
    |> Sintoma.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a sintoma.

  ## Examples

      iex> update_sintoma(sintoma, %{field: new_value})
      {:ok, %Sintoma{}}

      iex> update_sintoma(sintoma, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_sintoma(%Sintoma{} = sintoma, attrs) do
    sintoma
    |> Sintoma.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a sintoma.

  ## Examples

      iex> delete_sintoma(sintoma)
      {:ok, %Sintoma{}}

      iex> delete_sintoma(sintoma)
      {:error, %Ecto.Changeset{}}

  """
  def delete_sintoma(%Sintoma{} = sintoma) do
    Repo.delete(sintoma)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking sintoma changes.

  ## Examples

      iex> change_sintoma(sintoma)
      %Ecto.Changeset{data: %Sintoma{}}

  """
  def change_sintoma(%Sintoma{} = sintoma, attrs \\ %{}) do
    Sintoma.changeset(sintoma, attrs)
  end

  alias DiagnosticoMedico.Diagnostico.Enfermedad

   
  @doc """
  Returns the list of enfermedades.

  ## Examples

      iex> list_enfermedades()
      [%Enfermedad{}, ...]

  """
  def list_enfermedades do
    Repo.all(Enfermedad)
  end

  @doc """
  Gets a single enfermedad.

  Raises `Ecto.NoResultsError` if the Enfermedad does not exist.

  ## Examples

      iex> get_enfermedad!(123)
      %Enfermedad{}

      iex> get_enfermedad!(456)
      ** (Ecto.NoResultsError)

  """
  def get_enfermedad!(id), do: Repo.get!(Enfermedad, id)

  @doc """
  Creates a enfermedad.

  ## Examples

      iex> create_enfermedad(%{field: value})
      {:ok, %Enfermedad{}}

      iex> create_enfermedad(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_enfermedad(attrs \\ %{}) do
    %Enfermedad{}
    |> Enfermedad.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a enfermedad.

  ## Examples

      iex> update_enfermedad(enfermedad, %{field: new_value})
      {:ok, %Enfermedad{}}

      iex> update_enfermedad(enfermedad, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_enfermedad(%Enfermedad{} = enfermedad, attrs) do
    enfermedad
    |> Enfermedad.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a enfermedad.

  ## Examples

      iex> delete_enfermedad(enfermedad)
      {:ok, %Enfermedad{}}

      iex> delete_enfermedad(enfermedad)
      {:error, %Ecto.Changeset{}}

  """
  def delete_enfermedad(%Enfermedad{} = enfermedad) do
    Repo.delete(enfermedad)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking enfermedad changes.

  ## Examples

      iex> change_enfermedad(enfermedad)
      %Ecto.Changeset{data: %Enfermedad{}}

  """
  def change_enfermedad(%Enfermedad{} = enfermedad, attrs \\ %{}) do
    Enfermedad.changeset(enfermedad, attrs)
  end

  alias DiagnosticoMedico.Diagnostico.EnfermedadSintoma

  def get_enfermedades_por_sintomas(sintomas_names) do
    from(e in Enfermedad,
      join: es in EnfermedadSintoma,
      on: es.enfermedad_id == e.id,
      join: s in Sintoma,
      on: s.id == es.sintoma_id,
      where: s.name in ^sintomas_names,
      group_by: e.id,
      having: count(s.id) == ^length(sintomas_names),
      select: e
    )
    |> Repo.all()
  end


  @doc """
  Returns the list of enfermedades_sintomas.

  ## Examples

      iex> list_enfermedades_sintomas()
      [%EnfermedadSintoma{}, ...]

  """
  def list_enfermedades_sintomas do
    Repo.all(EnfermedadSintoma)
  end

  @doc """
  Gets a single enfermedad_sintoma.

  Raises `Ecto.NoResultsError` if the Enfermedad sintoma does not exist.

  ## Examples

      iex> get_enfermedad_sintoma!(123)
      %EnfermedadSintoma{}

      iex> get_enfermedad_sintoma!(456)
      ** (Ecto.NoResultsError)

  """
  def get_enfermedad_sintoma!(id), do: Repo.get!(EnfermedadSintoma, id)

  @doc """
  Creates a enfermedad_sintoma.

  ## Examples

      iex> create_enfermedad_sintoma(%{field: value})
      {:ok, %EnfermedadSintoma{}}

      iex> create_enfermedad_sintoma(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_enfermedad_sintoma(attrs \\ %{}) do
    %EnfermedadSintoma{}
    |> EnfermedadSintoma.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a enfermedad_sintoma.

  ## Examples

      iex> update_enfermedad_sintoma(enfermedad_sintoma, %{field: new_value})
      {:ok, %EnfermedadSintoma{}}

      iex> update_enfermedad_sintoma(enfermedad_sintoma, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_enfermedad_sintoma(%EnfermedadSintoma{} = enfermedad_sintoma, attrs) do
    enfermedad_sintoma
    |> EnfermedadSintoma.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a enfermedad_sintoma.

  ## Examples

      iex> delete_enfermedad_sintoma(enfermedad_sintoma)
      {:ok, %EnfermedadSintoma{}}

      iex> delete_enfermedad_sintoma(enfermedad_sintoma)
      {:error, %Ecto.Changeset{}}

  """
  def delete_enfermedad_sintoma(%EnfermedadSintoma{} = enfermedad_sintoma) do
    Repo.delete(enfermedad_sintoma)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking enfermedad_sintoma changes.

  ## Examples

      iex> change_enfermedad_sintoma(enfermedad_sintoma)
      %Ecto.Changeset{data: %EnfermedadSintoma{}}

  """
  def change_enfermedad_sintoma(%EnfermedadSintoma{} = enfermedad_sintoma, attrs \\ %{}) do
    EnfermedadSintoma.changeset(enfermedad_sintoma, attrs)
  end
end
