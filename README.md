# Diagnostico Medico

## PreRequisitos
- Docker
- NodeJS
- Elixir
- Phoenix
- Erlang

## Pasos de ejecucion

### 1. Configurar la DB

#### Si es la primera vez

1. Ejecutamos en terminal:
```
docker run --name phoenix-postgres -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -p 5500:5432 -d postgres
```

2. Nos ubicamos en la carpeta `diagnostico_medico` y ejecutamos en terminal:
```
mix ecto.migrate
```

#### Sino es la primera vez
Nos asegurmos que el contenedor este ejecutando:
```
docker start phoenix-postgres
```

### 2. Iniciar el Backend
Nos ubicamos en la carpeta `diagnostico_medico` y ejecutamos en terminal:
```
mix phx.server
```
### 3. Insertar los datos iniciales (solo la primera vez)
Nos ubicamos en la carpeta `consultas` y ejecutamos en terminal:
```
npm run cargarPrimitivos
npm run cargarRelaciones
```

Si da un error, ejecutar:
```
mix ecto.reset
```
Y volver al inicio de este punto.

### 4. Iniciar el Frontend 
Nos ubicamos en la carpeta `diagnostico_medico_frontend` y ejecutamos en terminal:
```
npm run dev
```
Entramos a la pagina por defecto:
[Localhost 5173](http://localhost:5173/)
