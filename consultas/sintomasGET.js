const fetch = require("node-fetch");

async function obtenerSintomas() {
  try {
    const response = await fetch("http://localhost:4000/api/sintoma");
    if (!response.ok) {
      throw new Error("Error en la petición: " + response.statusText);
    }
    const sintomas = await response.json();
    return sintomas;
  } catch (error) {
    throw error; // Re-throw the error to be handled by the calling function
  }
}

module.exports = obtenerSintomas;
