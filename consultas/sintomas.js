const fetch = require("node-fetch");

const sintomas = [
  { sintoma: { name: "Tos" } },
  { sintoma: { name: "Dolor de cabeza" } },
  { sintoma: { name: "Fatiga" } },
  { sintoma: { name: "Dolor de garganta" } },
  { sintoma: { name: "Náuseas" } },
  { sintoma: { name: "Dificultad para respirar" } },
  { sintoma: { name: "Pérdida de apetito" } },
  { sintoma: { name: "Mareos" } },
  { sintoma: { name: "Fiebre" } },
  { sintoma: { name: "Erupción cutánea" } },
];

const url = "http://localhost:4000/api/sintoma";

sintomas.forEach(async (sintoma) => {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(sintoma),
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();
    console.log("Success:", data);
  } catch (error) {
    console.error("Error:", error);
  }
});
