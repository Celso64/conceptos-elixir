const fetch = require("node-fetch");

const enfermedades = [
  { enfermedad: { name: "Resfriado común" } },
  { enfermedad: { name: "Gripe" } },
  { enfermedad: { name: "Migraña" } },
  { enfermedad: { name: "Diabetes" } },
  { enfermedad: { name: "COVID-19" } },
  { enfermedad: { name: "Asma" } },
  { enfermedad: { name: "Gastritis" } },
  { enfermedad: { name: "Depresión" } },
  { enfermedad: { name: "Hipertensión" } },
  { enfermedad: { name: "Alergias" } },
];

const url = "http://localhost:4000/api/enfermedad";

enfermedades.forEach(async (enfermedad) => {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(enfermedad),
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();
    console.log("Success:", data);
  } catch (error) {
    console.error("Error:", error);
  }
});
