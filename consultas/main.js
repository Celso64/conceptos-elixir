const obtenerSintomas = require("./sintomasGET");
const obtenerEnfermedades = require("./enfermedadesGET");
const EYS = require("./s_e");

const url = "http://localhost:4000/api/enfermedadsintoma";

async function subir(sintoma) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(sintoma),
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();
    console.log("Success:", data);
  } catch (error) {
    console.error("Error:", error);
  }
}

async function main() {
  try {
    const sintomas = await obtenerSintomas();
    const enfermedades = await obtenerEnfermedades();

    const sintomasMap = sintomas.data.reduce((map, sintoma) => {
      map[sintoma.name] = sintoma.id;
      return map;
    }, {});

    const enfermedadesMap = enfermedades.data.reduce((map, enfermedad) => {
      map[enfermedad.name] = enfermedad.id;
      return map;
    }, {});

    const output = [];

    EYS.forEach((x) => {
      x.enfermedad.sintomas.forEach((y) => {
        const sintoma = {
          enfermedad_id: enfermedadesMap[x.enfermedad.name],
          sintoma_id: sintomasMap[y.name_sintoma],
        };
        const res = {
          enfermedad_sintoma: sintoma,
        };
        console.log(res);
        subir(res);
      });
    });
  } catch (error) {
    console.error("Error al obtener los síntomas en main:", error);
  }
}

main();
