const enfermedadesYSintomas = [
  {
    enfermedad: {
      name: "Resfriado común",
      sintomas: [
        { name_sintoma: "Dolor de garganta" },
        { name_sintoma: "Tos" },
        { name_sintoma: "Fiebre" },
      ],
    },
  },
  {
    enfermedad: {
      name: "Gripe",
      sintomas: [
        { name_sintoma: "Fiebre" },
        { name_sintoma: "Tos" },
        { name_sintoma: "Dolor de cabeza" },
        { name_sintoma: "Fatiga" },
      ],
    },
  },
  {
    enfermedad: {
      name: "Migraña",
      sintomas: [
        { name_sintoma: "Dolor de cabeza" },
        { name_sintoma: "Náuseas" },
        { name_sintoma: "Mareos" },
      ],
    },
  },
  {
    enfermedad: {
      name: "Diabetes",
      sintomas: [
        { name_sintoma: "Fatiga" },
        { name_sintoma: "Pérdida de apetito" },
      ],
    },
  },
  {
    enfermedad: {
      name: "COVID-19",
      sintomas: [
        { name_sintoma: "Fiebre" },
        { name_sintoma: "Tos" },
        { name_sintoma: "Fatiga" },
        { name_sintoma: "Dificultad para respirar" },
      ],
    },
  },
  {
    enfermedad: {
      name: "Asma",
      sintomas: [
        { name_sintoma: "Dificultad para respirar" },
        { name_sintoma: "Tos" },
      ],
    },
  },
  {
    enfermedad: {
      name: "Gastritis",
      sintomas: [
        { name_sintoma: "Náuseas" },
        { name_sintoma: "Dolor de estómago" },
      ],
    },
  },
  {
    enfermedad: {
      name: "Depresión",
      sintomas: [
        { name_sintoma: "Fatiga" },
        { name_sintoma: "Pérdida de apetito" },
        { name_sintoma: "Mareos" },
      ],
    },
  },
  {
    enfermedad: {
      name: "Hipertensión",
      sintomas: [
        { name_sintoma: "Dolor de cabeza" },
        { name_sintoma: "Mareos" },
      ],
    },
  },
  {
    enfermedad: {
      name: "Alergias",
      sintomas: [{ name_sintoma: "Erupción cutánea" }, { name_sintoma: "Tos" }],
    },
  },
];

module.exports = enfermedadesYSintomas;
